import { NestFactory } from "@nestjs/core";
import { AppModule } from "app";
import * as dotenv from "dotenv";
import { ConfigService } from "./shared/config/config.service";

dotenv.config();

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);

  await app.listen(configService.Port);
}
bootstrap();
