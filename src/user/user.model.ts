import { Typegoose, prop, ModelType } from "typegoose";
import { UserRole } from "./user-roles.enum";
import { schemaOptions } from "../shared/base";

export class User extends Typegoose {
  @prop({
    required: [true, "Username is required"],
    unique: true,
    minlength: [6, "Must be at least 6 characters"]
  })
  public name: string;

  @prop({
    required: [true, "Password is required"],
    minlength: [6, "Must be at least 6 characters"]
  })
  public password: string;

  @prop({
    required: [true, "Email is required"]
  })
  public email: string;

  @prop()
  public firstName?: string;

  @prop()
  public lastName?: string;

  @prop({ enum: UserRole, default: UserRole.Standard })
  public role?: UserRole;

  @prop({ default: Date.now() })
  public createdAt?: Date;

  @prop({ default: Date.now() })
  public updatedAt?: Date;

  public id?: string;

  @prop()
  public get FullName(): string {
    return `${this.firstName} ${this.lastName}`;
  }

  static get Model(): ModelType<User> {
    return new User().getModelForClass(User, { schemaOptions });
  }

  static get ModelName(): string {
    return this.Model.modelName;
  }
}
