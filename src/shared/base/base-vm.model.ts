import { ApiModelPropertyOptional } from "@nestjs/swagger";
import { SchemaOptions } from "mongoose";

export class BaseModel {
  @ApiModelPropertyOptional({ type: String, format: "date-time" })
  public createdAt?: Date;

  @ApiModelPropertyOptional({ type: String, format: "date-time" })
  public updatedAt?: Date;

  @ApiModelPropertyOptional()
  public id?: string;
}

export const schemaOptions: SchemaOptions = {
  toJSON: {
    virtuals: true,
    getters: true
  }
};
