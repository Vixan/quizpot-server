import * as Joi from "joi";
import * as fs from "fs";
import * as dotenv from "dotenv";

export interface IEnvConfig {
  [key: string]: string;
}

export class ConfigService {
  private readonly envConfig: IEnvConfig;

  constructor(filePath: string) {
    console.log(filePath);

    const config = dotenv.parse(fs.readFileSync(filePath));
    this.envConfig = this.validateInput(config);
  }

  /**
   * Ensures all needed variables are set, and returns the validated JavaScript object
   * including the applied default values.
   */
  private validateInput(envConfig: IEnvConfig): IEnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      NODE_ENV: Joi.string()
        .valid(["development", "production", "test", "provision"])
        .default("development"),
      PORT: Joi.number().default(3000)
    });

    const { error, value: validatedEnvConfig } = Joi.validate(
      envConfig,
      envVarsSchema
    );

    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }

    return validatedEnvConfig;
  }

  public get Port(): number {
    return Number(this.envConfig.PORT);
  }
}
