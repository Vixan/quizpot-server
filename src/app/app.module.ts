import { Module } from "@nestjs/common";
import { AppController } from "app/app.controller";
import { AppService } from "./app.service";
import { ConfigModule } from "../shared/config";

@Module({
  imports: [ConfigModule],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
